package com.example.miniproject.model.repository;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.LiveData;

import com.example.miniproject.model.AppDatabase;
import com.example.miniproject.model.Country;
import com.example.miniproject.model.CountryDao;
import com.example.miniproject.model.CountryRoom;
import com.example.miniproject.model.retrofit.RetrofitInstance;
import com.google.android.material.snackbar.Snackbar;
import com.example.miniproject.model.AppDatabase;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountryRepository {
    public CountryDao countryDao;
    private RetrofitInstance retrofitInstance;
    public List<CountryRoom> allCountry;

    public interface requestCallback{
        void onSuccess(List<CountryRoom> countries);
        void onFailed(String message);
    }

    public void getCountryData(requestCallback callback, Context context){
        //query ke room getallcountry
        AppDatabase database = AppDatabase.getDatabase(context); // context = null
        countryDao = database.countryDao();
        allCountry = countryDao.getAll();

        if(allCountry.size() <= 0 || allCountry == null) {
            retrofitInstance = new RetrofitInstance();
            retrofitInstance
                    .getAPI()
                    .getCountries()
                    .enqueue(new Callback<List<Country>>() {
                        @Override
                        public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                            List<CountryRoom> countryRooms = new ArrayList<>();
                            for (int i = 0; i < response.body().size(); i++) {
                                Country country = response.body().get(i);
                                CountryRoom countryRoom = new CountryRoom();
                                countryRoom.country = country.getCountry();
                                countryRoom.continent = country.getContinent();
                                countryRoom.cases = country.getCases();
                                countryRoom.todayCases = country.getTodayCases();
                                countryRoom.recovered = country.getRecovered();
                                countryRoom.todayRecovered = country.getTodayRecovered();
                                countryRoom.deaths = country.getDeaths();
                                countryRoom.todayDeaths = country.getTodayDeaths();
                                insert(countryRoom);
                            }
                            callback.onSuccess(countryRooms);
                        }

                        @Override
                        public void onFailure(Call<List<Country>> call, Throwable t) {
                            callback.onFailed(t.getMessage());
                        }
                    });
        }
        else{
            callback.onSuccess(allCountry);
        }
    }

    public void insert(CountryRoom country) {
        AppDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                countryDao.insert(country);
            }
        });
    }

    /*
    public boolean checkRoom(){
     //panggil method DAO getAllCountry
        //if()
        checkroom.setValue(true);
    }*/
}
