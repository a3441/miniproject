package com.example.miniproject.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class CountryRoom {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "country")
    public String country;
    @ColumnInfo(name = "continent")
    public String continent;
    @ColumnInfo(name = "cases")
    public Integer cases;
    @ColumnInfo(name = "todayCases")
    public Integer todayCases;
    @ColumnInfo(name = "deaths")
    public Integer deaths;
    @ColumnInfo(name = "todayDeaths")
    public Integer todayDeaths;
    @ColumnInfo(name = "recovered")
    public Integer recovered;
    @ColumnInfo(name = "todayRecovered")
    public Integer todayRecovered;
}
