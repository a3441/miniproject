package com.example.miniproject.model.retrofit;

import com.example.miniproject.model.Country;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MyApiEndPointInterface {
    String BASE_URL = "https://corona.lmao.ninja/v2/";

    @GET("countries?sort=country")
    Call<List<Country>> getCountries();

    @GET("countries/{country}")
    Call<Country> getUser(@Path("country") String iso3);
}
