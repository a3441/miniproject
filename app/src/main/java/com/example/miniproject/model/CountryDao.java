package com.example.miniproject.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface CountryDao {
    @Query("SELECT * FROM CountryRoom")
    List<CountryRoom> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CountryRoom country);
}
