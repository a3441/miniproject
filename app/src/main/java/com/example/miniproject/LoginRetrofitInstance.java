package com.example.miniproject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginRetrofitInstance {
    private LoginApiEndPointInterface API;

    private static Retrofit retrofit = null;


    public static Retrofit getClient(String baseurl){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseurl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofit.create(LoginApiEndPointInterface.class);
        return retrofit;
    }

}
