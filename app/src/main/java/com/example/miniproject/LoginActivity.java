package com.example.miniproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.miniproject.session.SessionManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity{
    TextInputLayout tInputEdit_username, tInputEdit_password;
    LoginApiEndPointInterface loginApiEndPointInterface;
    String username,password;
    Button btn_login;
    SessionManager sessionManager = new SessionManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tInputEdit_username = findViewById(R.id.TextInputLayoutUsername);
        tInputEdit_password = findViewById(R.id.TextInputLayoutPassword);

        btn_login = findViewById(R.id.id_btn_LoginUser);

        /*RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", username)
                .addFormDataPart("password", password)
                .build();

         */
        loginApiEndPointInterface = ApiUtils.getAPIService();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                loginPresenter.onLoginButtonClick(tInputEdit_username.getText().toString(), tInputEdit_password.getText().toString());
                username = tInputEdit_username.getEditText().getText().toString();
                password = tInputEdit_password.getEditText().getText().toString();
                loginApiEndPointInterface.postLoginUser(username, password).enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        if (response.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "Call Successfull", Toast.LENGTH_LONG).show();
                            sessionManager.storeUserToken(LoginActivity.this, response.body().getToken());
                            intentToHomeActivity();
                        }else {
                            Toast.makeText(LoginActivity.this, "Call Failed", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, "Call Failed" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    public void intentToHomeActivity(){
        Intent intent = new Intent(this, CountryActivity.class);
        startActivity(intent);
    }
}