package com.example.miniproject.viewmodel;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.miniproject.model.Country;
import com.example.miniproject.model.CountryRoom;
import com.example.miniproject.model.repository.CountryRepository;

import java.util.List;

public class CountryViewModel extends AndroidViewModel {
    private CountryRepository countryRepository;
    private Application application;
    public MutableLiveData<List<CountryRoom>> allCountries = new MutableLiveData<>();

    public CountryViewModel(@NonNull Application application) {
        super(application);
        countryRepository = new CountryRepository();
        this.application = application;
    }

    public void getCountryData(Context context){
        countryRepository.getCountryData(new CountryRepository.requestCallback() {
            @Override
            public void onSuccess(List<CountryRoom> countries) {
                allCountries.setValue(countries);
            }

            @Override
            public void onFailed(String message) {
                allCountries.setValue(null);
            }
        }, context);
    }

}
