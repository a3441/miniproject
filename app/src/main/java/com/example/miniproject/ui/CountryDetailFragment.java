package com.example.miniproject.ui;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.miniproject.R;
import com.example.miniproject.model.Country;
import com.example.miniproject.model.CountryRoom;
import com.example.miniproject.viewmodel.CountryViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CountryDetailFragment extends Fragment {
    private static CountryRoom countryDetail;
    private TextView tvcountry;
    private TextView tvcontinent;
    private TextView tvtotalcase;
    private TextView tvtodaycase;
    private TextView tvtotaldeath;
    private TextView tvtodaydeath;
    private TextView tvtotalheal;
    private TextView tvtodayheal;

    public static CountryDetailFragment newInstance(CountryRoom country) {
        countryDetail = country;
        return new CountryDetailFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //countryViewModel = new ViewModelProvider(requireActivity()).get(CountryViewModel.class);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.country_detail_layout, container, false);
        tvcountry = view.findViewById(R.id.tv_country_name);
        tvcontinent = view.findViewById(R.id.tv_continent_name);
        tvtotalcase = view.findViewById(R.id.tv_total_case);
        tvtodaycase = view.findViewById(R.id.tv_today_case);
        tvtotaldeath = view.findViewById(R.id.tv_total_death);
        tvtodaydeath = view.findViewById(R.id.tv_today_death);
        tvtotalheal = view.findViewById(R.id.tv_total_heal);
        tvtodayheal = view.findViewById(R.id.tv_today_heal);

        //btn_update_employee = view.findViewById(R.id.roomSaveUser);

        setValue();

        return view;
    }

    public void setValue(){
        tvcountry.setText(countryDetail.country);
        tvcontinent.setText(countryDetail.continent);
        tvtotalcase.setText(countryDetail.cases.toString());
        tvtodaycase.setText(countryDetail.todayCases.toString());
        tvtotaldeath.setText(countryDetail.deaths.toString());
        tvtodaydeath.setText(countryDetail.todayDeaths.toString());
        tvtotalheal.setText(countryDetail.recovered.toString());
        tvtodayheal.setText(countryDetail.todayRecovered.toString());
    }

}
