package com.example.miniproject.ui;

import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.miniproject.R;
import com.example.miniproject.model.Country;
import com.example.miniproject.model.CountryRoom;
import com.example.miniproject.viewmodel.CountryViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class CountryFragment extends Fragment {
    private CountryViewModel countryViewModel;
    private FloatingActionButton fab;
    private RecyclerView recyclerView;
    private CountryAdapter countryAdapter;


    private final CountryClickableCallback countryClickableCallback = new CountryClickableCallback() {
        @Override
        public void onClick(View view, CountryRoom country) {
            //DialogFragment newFragment = DeleteEmployeeDialogFragment.newInstance(employee);
            //newFragment.show(getChildFragmentManager(), "DeleteEmployeeDialogFragment");

            //panggil fragment detail
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_every_day, CountryDetailFragment.newInstance(country))
                    .commitNow();
        }
    };

    public static CountryFragment newInstance(){
        return new CountryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        countryViewModel = new ViewModelProvider(requireActivity()).get(CountryViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview_country, container, false);
        recyclerView = view.findViewById(R.id.roomRecyclerView);
        countryAdapter = new CountryAdapter(countryClickableCallback);
        recyclerView.setAdapter(countryAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        countryViewModel.getCountryData(requireActivity());

        countryViewModel.allCountries.observe(getViewLifecycleOwner(), countries -> {
            if (countries != null) {
                countryAdapter.submitList(countries);
            }
        });
    }


}
