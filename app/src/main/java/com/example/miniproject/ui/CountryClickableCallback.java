package com.example.miniproject.ui;

import android.view.View;

import com.example.miniproject.model.Country;
import com.example.miniproject.model.CountryRoom;

public interface CountryClickableCallback {
    void onClick(View view, CountryRoom country);
}
