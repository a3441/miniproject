package com.example.miniproject.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.miniproject.R;
import com.example.miniproject.model.Country;
import com.example.miniproject.model.CountryRoom;

import java.util.List;

public class CountryAdapter extends ListAdapter<CountryRoom, CountryAdapter.ViewHolder> {

    private final CountryClickableCallback countryClickableCallback;

    protected CountryAdapter(@NonNull CountryClickableCallback countryClickableCallback) {
        super(new AsyncDifferConfig.Builder<>(new DiffUtil.ItemCallback<CountryRoom>() {
            @Override
            public boolean areItemsTheSame(@NonNull CountryRoom oldItem, @NonNull CountryRoom newItem) {
                return oldItem.uid == newItem.uid;
            }

            @Override
            public boolean areContentsTheSame(@NonNull CountryRoom oldItem, @NonNull CountryRoom newItem) {
                return oldItem.uid == (newItem.uid)
                        && oldItem.country.equals(newItem.country)
                        && oldItem.continent.equals(newItem.continent);
            }
        }).build());
        this.countryClickableCallback = countryClickableCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recyclerview_country_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvCountry.setText(getItem(position).country);
        holder.tvContinent.setText(getItem(position).continent);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvCountry;
        TextView tvContinent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCountry = itemView.findViewById(R.id.room_tv_country);
            tvContinent = itemView.findViewById(R.id.room_tv_continent);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
                CountryRoom country = getItem(position);
                countryClickableCallback.onClick(v, country);
            }
        }
    }
}
