package com.example.miniproject;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginApiEndPointInterface {
    // Trailing slash is needed
    String BASE_URL = "https://talentpool.oneindonesia.id";

    @POST("/api/user/login")
    @FormUrlEncoded
    @Headers("X-API-KEY:454041184B0240FBA3AACD15A1F7A8BB")
    Call<UserResponse> postLoginUser(@Field("username") final String username, @Field("password") final String password);
}
