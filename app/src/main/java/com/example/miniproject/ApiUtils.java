package com.example.miniproject;

public class ApiUtils {
    private ApiUtils() {}

    public static final String BASE_URL = "https://talentpool.oneindonesia.id";

    public static LoginApiEndPointInterface getAPIService() {

        return LoginRetrofitInstance.getClient(BASE_URL).create(LoginApiEndPointInterface.class);
    }
}
